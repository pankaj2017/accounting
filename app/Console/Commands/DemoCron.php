<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Account;

class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");
          
        $startDateNew1 = Carbon::today()->addDays(3);
        $today = Carbon:: today();
        $accounts = Account::where('revise_remind', $startDateNew1)
                            ->where('is_verify',1)
                            ->where('status',1)
                            ->get();
        foreach ($accounts as $key => $value) {
            $account = Account::where('id',$value->id)->first();
            $account->payment_status = 0;
            $account->save();
             \Log::info('completed');
        }
        return Command::SUCCESS;
    }
}
