<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Excel as BaseExcel;
use App\Exports\AccountExport;
use Illuminate\Support\Facades\Mail;

class OneDaySend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:onedaysend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $filename = date("d/m/Y")."_backup.xlsx";
        $attachment = Excel::raw(new AccountExport, BaseExcel::XLSX);
        $subject = "Dialy Client Backup";

        Mail::raw("This is automatically generated Daily Update", function($message) use($filename,$attachment, $subject)
        {
            $message->from('info@mkvfinance.com');
            $message->to('kitanpatel@gmail.com')->subject($subject);
            $message->attachData($attachment, $filename);
        });

        $this->info('Excel backup has been send successfully');
        return Command::SUCCESS;
    }
}
