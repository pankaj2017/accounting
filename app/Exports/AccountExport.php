<?php

namespace App\Exports;

use App\Models\Account;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AccountExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Account::all();
    }

    public function map($row): array
    {
        return [
            $row->company->name?? "",
            $row->name,
            $row->mobile,
            $row->other_mobile,
            $row->email,
            $row->agent->name ?? "",
            $row->agent_mobile,
            $row->fund,
            $row->charge,
            $row->bank->name ?? "",
            $row->branch,
            $row->finance_date,
            $row->finance_rate,
            $row->finance_duration,
            $row->type,
            $row->note
        ]; 
     }

    public function headings(): array
    {
        return [
            'Company Name',
            'Name',
            'Mobile',
            'Other Mobile',
            'Email',
            'Agent name',
            'Agent Mobile',
            'Fund',
            'Charge',
            'Bank',
            'Branch',
            'Finance Date',
            'Finance Rate',
            'Finance Duration',
            'Type',
            'Note',
        ];
    }
}
