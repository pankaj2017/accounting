<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Account;
use App\Models\Agent;
use App\Models\Company;
use App\Models\Bank;
use DataTables;
use Hash;
use Carbon\Carbon;
use App\Mail\OtpEmail;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportAccount;
use App\Exports\AccountExport;
use PDF;
Use Image;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Account::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                           $btn = '<a href="'. route('account.edit', $row->id) .'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="'. route('account.show', $row->id) .'" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a> <a href="#" data-account_id="'.$row->id.'" class="update_payment_status" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-cog text-inverse m-r-10"></i> </a> <a href="#" data-account_id="'.$row->id.'" class="revise_payment_notify" data-toggle="tooltip" data-original-title="Update Payment Option"> <i class="fa fa-bank text-inverse m-r-10"></i> </a> <a href="#" onClick=deleteacc("'.$row->id.'") data-account_id="'.$row->id.'" class="deleteacc" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-trash text-inverse m-r-10"></i> </a>';
    
                            return $btn;
                    })
                    ->addColumn('created_at', function($row){
                           $btn = date('d-m-Y H:i:s', strtotime($row->created_at));
    
                            return $btn;
                    })
                    ->addColumn('verify', function($row){
                            if($row->is_verify) {
                                $verify = '<span class="badge badge-info">Verified</span>';
                            } else {
                                $verify = '<span class="badge badge-warning">Not verified</span>';
                            }
                            return $verify;
                    })
                    ->addColumn('payment_status', function($row){
                            if($row->payment_status == 0) {
                                $payment_status = '<span class="badge badge-warning">Un Paid</span>';
                            } elseif($row->payment_status == 1) {
                                $payment_status = '<span class="badge badge-success">Paid</span>';
                            } elseif($row->payment_status == 2) {
                                $payment_status = '<span class="badge badge-info">By Pass</span>';
                            }
                            return $payment_status;
                    })
                    ->addColumn('agent_name', function($row){
                            return $row->agent->name ?? "-";
                    })
                    ->addColumn('status', function($row){
                            if($row->status) {
                                $status = '<a data-toggle="tooltip" data-placement="left" data-original-title=" Completed Status" class="label label-primary" style="color:#f0f0f0;" aria-describedby="tooltip659663">Completed</a>';
                            } else {
                                $status = '<a data-toggle="tooltip" data-placement="left" data-original-title=" In Progress Status" class="label label-danger" style="color:#f0f0f0;" aria-describedby="tooltip659663">In Progress</a>';
                            }
                            return $status;
                    })
                    ->escapeColumns('verify', 'status','payment_status')
                    ->rawColumns(['action', 'created_at',' status'])
                    ->make(true);
        }
        
        return view('admin.accounts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = Account::where('id',$id)->first();
        
        return view('admin.accounts.show',compact('account'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::where('id',$id)->first();
        $agents = Agent::where('status' ,1)->pluck('name','id',)->toArray();
        $companies = Company::where('status' ,1)->pluck('name','id',)->toArray();
        $banks = Bank::where('status' ,1)->pluck('name','id',)->toArray();
        // dd($account);
         // $p = Carbon::createFromFormat('Y-m-d', $account->finance_date)->format('m-d-Y');; 
         // $account->finance_date = $p;
         // dd($account->company);
        return view('admin.accounts.edit',compact('account','agents', 'companies', 'banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        // dd(Carbon::parse('2019-01-12'));

        $this->validate($request,[
            'company_id'=>'required',
            'name'=>'required',
            'agent_name'=>'required',
            'agent_mobile'=>'required',
            'bank_name'=>'required',
            'branch'=>'required',
        ]);
        
        $account = Account::where('id',$id)->first();
        // $dirPath = public_path('images/'.$id.'/'); 
        // if (!file_exists($dirPath)) {
        //     mkdir($dirPath, 0777, true);
        // }
        // $aadhaarFile = null;
        // if (request()->hasFile('aadhaar_card')) {
        //     $file = request()->file('aadhaar_card');
        //     $aadhaarFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     // open an image file
        //     $file = \Image::make($file->getRealPath())->resize(400, 300);
       
        //     // $file->move($dirPath, $aadhaarFile);   
        //     $file->save($dirPath.$aadhaarFile);
        //     $account->aadhaar_card = $aadhaarFile;
        //     // die("check");
        // }        
        // $pancardFile = null;
        // if (request()->hasFile('pan_card')) {
        //     $file = request()->file('pan_card');
        //     $pancardFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file = \Image::make($file->getRealPath())->resize(400, 300);
        //     $file->save($dirPath.$pancardFile);
        //     //$file->move($dirPath, $pancardFile);    
        //     $account->pan_card = $pancardFile;
        // }
        // $passportFile = null;
        // if (request()->hasFile('passport_card')) {
        //     $file = request()->file('passport_card');
        //     $passportFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file = \Image::make($file->getRealPath())->resize(400, 300);
        //     $file->save($dirPath.$passportFile);
        //     // $file->move($dirPath, $passportFile);    
        //     $account->passport_card = $passportFile;
        // } 
        // $photoFile = null;
        // if (request()->hasFile('photo')) {
        //     $file = request()->file('photo');
        //     $photoFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file = \Image::make($file->getRealPath())->resize(400, 300);
        //     $file->save($dirPath.$photoFile);
        //     // $file->move($dirPath, $photoFile);    
        //     $account->photo = $photoFile;
        // }
        // $otherFile = null;
        // if (request()->hasFile('other')) {
        //     $file = request()->file('other');
        //     $otherFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file = \Image::make($file->getRealPath())->resize(400, 300);
        //     $file->save($dirPath.$otherFile);
        //     // $file->move($dirPath, $otherFile);    
        //     $account->other = $otherFile;
        // }
        // dd($request->all());
        $account->company_id = $request->get('company_id');
        $account->name = $request->get('name');
        $account->agent_name = $request->get('agent_name');
        $account->agent_mobile = $request->get('agent_mobile');
        $account->other_mobile = $request->get('other_mobile');
        $account->bank_name = $request->get('bank_name');
        $account->branch = $request->get('branch');
        $account->fund = $request->get('fund');
        $account->charge = $request->get('charge');
        $account->finance_date = $request->get('finance_date');
        $account->finance_rate = $request->get('finance_rate');
        $account->finance_duration = $request->get('finance_duration');
        $account->type = $request->get('type');
        $account->note = $request->get('note');
        $account->other_mobile = $request->get('other_mobile');
        $account->status = 1;
        if(!empty($request->get('pay_option'))) {
            $account->pay_option = implode(",",$request->get('pay_option'));
        }

        
        $startDate1 = Carbon::createFromFormat('m-d-Y',$account->finance_date)->format('Y-m-d');
        $account->remind_date = Carbon::parse($startDate1)->addDays($request->get('finance_duration'));
        $account->save();

        return redirect()->route('account.index')
                         ->with('message','Record updated successfully')
                         ->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function customerAuth(Request $request) {
        $this->validate($request,[
            'mobile'=>'required',
            'email'=>'required|email',
        ],
        [
            'mobile.required' => 'The phone number field is required.'
        ]
        );
        // dd($request->get('id'));
        $accountObj = Account::find($request->get('id'));
        // dd($accountObj);
        echo $request->get('otp');
        if($accountObj->otp != $request->get('otp')) {
            return redirect()->back()
                        ->withInput($request->input())
                         ->with('message','Enter valiad otp')
                         ->with('message_type','error')->with('pn','check');
        }

        $accountObj->is_verify = 1;
        $accountObj->save();

        return redirect()->route('account.index')
                         ->with('message','Record inserted successfully')
                         ->with('message_type','success');
    }

    public function customerInformation(Request $request) {
        // dd($request->all());
        
        $this->validate($request,[
            'name'=>'required',
            'agent_name'=>'required',
            'agent_mobile'=>'required',
            'bank_name'=>'required',
            'branch'=>'required',
        ]);

        $account = new Account();
        $account->mobile = $request->get('phone');
        $account->email  = $request->get('email');
        $account->otp    = '123456';
        $account->save();

        // dd($account->id);
        $created_id = $account->id;
        return view('admin.accounts.create',compact('created_id'));
    }

    public function getOtp(Request $request) {
        $client = new \GuzzleHttp\Client();
       
        $account = new Account();
        $account->mobile = $request->get('mobile');
        $account->email  = $request->get('email');
        $account->otp    = random_int(100000, 999999);
        $account->save();

        $body['apikey'] = "fGCpEu9eo9Vu7g8p";
        $body['senderid'] = "AZACUS";
        $body['number'] = $account->mobile;
        $body['message'] = "Your OTP for MKVFinance mobile verification is $account->otp.";
        $response = $client->request('POST', 'http://msg.mtalkz.com/V2/http-api-post.php', [
            'body' => json_encode($body) 
        ]);
        $response = $response->getBody()->getContents();
   
        $data = array('name'=>"Mkv Finanace",'email'=>$account->email,'otp' => $account->otp);
   
        // \Mail::send(['text'=>'otp'], $data, function($message) use ($data) {
            
        //     $message->to($data['email'], 'Verify')->subject
        //         ('Authentication');
        //     $message->from('xyz@gmail.com',$data['name']);
        // });
        return response()->json(array('success' => true, 'message' => 'Send otp successfully','data'=>['id'=>$account->id]), 200);
        
    }

    public function importView() {
        return view('admin.accounts.import');
    }

    public function import(Request $request){
        
        // dd($request->all());
        Excel::import(new ImportAccount, $request->file('file')->store('files'));

        return redirect()->back()->with('message','Import successfully')
                         ->with('message_type','success');;
    }
    public function export() 
    {
        return Excel::download(new AccountExport, 'users.xlsx');
    }


    public function downloadPDF($id) {
        $account = Account::where('id',$id)->first();
        // dd($account->finance_rate);die;
          $data = [
            'id' => $account->id,
            'company_name' => $account->company->name ?? '',
            'title' => 'MKV Finance',
            'name' =>$account->name,
            'mobile' =>$account->mobile,
            'email' =>$account->email,
            'agent_name' =>$account->agent->name ?? '',
            'agent_mobile' =>$account->agent->mobile ?? '',
            'bank_name' =>$account->bank->name ?? '',
            'branch' =>$account->bank->branch ?? '',
            'finance_date' =>$account->finance_date,
            'finance_rate' =>$account->finance_rate,
            'fund' =>$account->fund,
            'charge' =>$account->charge,
            'date' => date('m/d/Y'),
            'aadhaar_card' => $account->aadhaar_card,
            'pan_card' => $account->pan_card,
            'passport_card' => $account->passport_card,
            'photo' => $account->photo,
            'other' => $account->other,
            'note' => $account->note
        ];

           
        $pdf = PDF::loadView('admin.accounts.testPDF', $data);
     
        return $pdf->download($account->name.'.pdf');
    }
    public function getPhone(Request $request) {
        $agentId = $request->get('agent_id');

        $agent = Agent::find($agentId);
        return response()->json(array('success' => true, 'message' => 'Phone get successfully','data'=>['mobile'=>$agent->mobile]), 200);
    }
    public function getBank(Request $request) {
        $bankId = $request->get('bank_id');

        $bank = Bank::find($bankId);
        // dd($bank);
        return response()->json(array('success' => true, 'message' => 'Phone get successfully','data'=>['branch'=>$bank->branch]), 200);
    }

    public function uploadCropImage(Request $request){
          // dd($request->all());
        // $images = $request->get('aadhaar_card');
        $id = $request->get('id');
        $account = Account::where('id',$id)->first();
        $dirPath = public_path('images/'.$id.'/'); 
        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0777, true);
        }
        $aadhaarFile = $pancardFile = $passportFile = $photoFile = $otherFile = null;

        if (request()->hasFile('aadhaar_card')) {
            $file = request()->file('aadhaar_card');
            $aadhaarFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension(); 
            $file->move($dirPath, $aadhaarFile);    
            $account->aadhaar_card = $aadhaarFile;
        }
        if (request()->hasFile('pan_card')) {
            $file = request()->file('pan_card');
            $pancardFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension(); 
            $file->move($dirPath, $pancardFile);    
            $account->pan_card = $pancardFile;
        }
        if (request()->hasFile('passport_card')) {
            $file = request()->file('passport_card');
            $passportFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move($dirPath, $passportFile);    
            $account->passport_card = $passportFile;
        } 
        if (request()->hasFile('photo')) {
            $file = request()->file('photo');
            $photoFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move($dirPath, $photoFile);    
            $account->photo = $photoFile;
        }
        if (request()->hasFile('other')) {
            $file = request()->file('other');
            $otherFile = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move($dirPath, $otherFile);    
            $account->other = $otherFile;
        } 
        $account->save();
        return response()->json(['status'=>true]);
    }

    public function paymentStatusGet(Request $request) {
        // die("D");
        // $people = People::find($request->get('people_id'));
        // dd($people->propertyPreference->property_type);
        $accountId = $request->get('account_id');
        $account = Account::where('id',$accountId)->first();
        $payment_status = $account->payment_status;
        $view = view("admin.update-status",compact('accountId', 'payment_status'))->render();
        return response()->json(['html'=>$view]);
    }
     public function paymentStatusPost(Request $request) {
        $account = Account::where('id',$request->get('account_id'))->first();
        $account->payment_status = $request->get('payment_status');
        $account->save();

        return response()->json(['success'=> true],200);
    }
    public function revisePaymentNotifyGet(Request $request) {
        // die("D");
        // $people = People::find($request->get('people_id'));
        // dd($people->propertyPreference->property_type);
        $accountId = $request->get('account_id');
        $account = Account::where('id',$accountId)->first();
        $revise_remind = $account->revise_remind;
        // dd($revise_remind);
        $view = view("admin.accounts.revise_payment",compact('accountId', 'account','revise_remind'))->render();
        return response()->json(['html'=>$view]);
    }
     public function revisePaymentNotifyPost(Request $request) {
        $account = Account::where('id',$request->get('account_id'))->first();
        $account->revise_remind = $request->get('revise_remind');
        $account->save();

        return response()->json(['success'=> true],200);
    }

    public function deleteAccount(Request $request) {
        // dd($request->all());
        $account = Account::where('id',$request->get('id'))->first();
        $account->delete();

        return response()->json(['success'=> true],200);
    }

}
