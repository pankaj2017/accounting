<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Account;
use Carbon\Carbon;


class AdminController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $startDate1 = Carbon::today()->subDay(1000);
        $startDateNew1 = Carbon::today()->addDays(15);
        $endDate = Carbon:: today();

        // 0 = unpaid
        // 1 = paid
        // 2 = By pass
        $accounts = Account::whereBetween('remind_date', [$startDate1, $startDateNew1])
                            // ->orWhereBetween('finance_date', [$startDate2, $endDate])
                            // ->orWhereBetween('finance_date', [$startDate3, $endDate])
                        
                            // ->orWhereBetween('finance_date', [$endDate,$startDateNew5])
                            ->where('is_verify',1)
                            ->where('status',1)
                            ->where('payment_status',0)
                            ->get();
       
        // dd($accounts);die;
        return view('admin.admin_dashboard',compact('accounts'));
    }

    public function reminder(Request $request) {
        $client = new \GuzzleHttp\Client();
        $account = Account::where('id',$request->get('account_id'))->first();
        $otherMobiles = $account->other_mobile ?? "";
        $agentMobile = $account->agent->mobile ?? "";
        
        $body['apikey'] = "fGCpEu9eo9Vu7g8p";
        $body['senderid'] = "AZACUS";
        $body['number'] = $account->mobile.','.$otherMobiles.','.$agentMobile;
        $body['tempid'] = "1007226199889286372";
        $body['message'] = "We would like to remind you that $account->fund sum is due for payment since $account->finance_date . -AZACUS";
        $response = $client->request('POST', 'http://msg.mtalkz.com/V2/http-api-post.php', [
            'body' => json_encode($body) 
        ]);
        $response = $response->getBody()->getContents();
        
        return response()->json(array('success' => true, 'message' => 'Send reminder message successfully','data'=>['id'=>$account->id]), 200);
    }
    
    public function reReminder(Request $request) {
        $client = new \GuzzleHttp\Client();
       
        $account = Account::where('id',$request->get('account_id'))->first();
        $otherMobiles = $account->other_mobile ?? "";
        $agentMobile = $account->agent->mobile ?? "";

        $body['apikey'] = "fGCpEu9eo9Vu7g8p";
        $body['senderid'] = "AZACUS";
        $body['number'] = $account->mobile.','.$otherMobiles.','.$agentMobile;
        $body['tempid'] = "1007260176273407362";
        $body['message'] = "Hello, $account->name despite our previous reminders, we have still not received the $account->fund since $account->finance_date . we will withdraw fund with immediate effect.- AZACUS";
        $response = $client->request('POST', 'http://msg.mtalkz.com/V2/http-api-post.php', [
            'body' => json_encode($body) 
        ]);
        $response = $response->getBody()->getContents();
      
        return response()->json(array('success' => true, 'message' => 'Send Re-reminder message successfully','data'=>['id'=>$account->id]), 200);
    }
}
