<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agent;
use DataTables;
use Hash;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Agent::select('*')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('agent.edit', $row->id) .'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> ';
                    return $btn;
                })
                ->addColumn('status', function($row){
                            if($row->status) {
                                $status = '<a data-toggle="tooltip" data-placement="left" data-original-title=" Active Status" class="label label-info" style="color:#f0f0f0;" aria-describedby="tooltip659663">Active</a>';
                            } else {
                                $status = '<a data-toggle="tooltip" data-placement="left" data-original-title=" In Active" class="label label-danger" style="color:#f0f0f0;" aria-describedby="tooltip659663">In Active</a>';
                            }
                            return $status;
                })
                ->escapeColumns('status')
                ->rawColumns(['action'])
                ->make(true);

        }
        
        return view('admin.agents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent = new Agent();
        $agent->name = $request->get('name');
        $agent->mobile  = $request->get('mobile');
        $agent->status  = 1;
        $agent->save();

        return redirect()->route('agent.index')
                         ->with('message','Insert record successfully')
                         ->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Agent::find($id);
        // dd($agent);die;
        return view('admin.agents.edit',compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::find($id);
        $agent->name = $request->get('name');
        $agent->mobile  = $request->get('mobile');
        $agent->status  = 1;
        $agent->save();

        return redirect()->route('agent.index')
                         ->with('message','Update record successfully')
                         ->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
