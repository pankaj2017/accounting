<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use DataTables;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Company::select('*')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('company.edit', $row->id) .'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> ';
                    return $btn;
                })
                ->addColumn('status', function($row){
                            if($row->status) {
                                $status = '<a data-toggle="tooltip" data-placement="left" data-original-title=" Active Status" class="label label-info" style="color:#f0f0f0;" aria-describedby="tooltip659663">Active</a>';
                            } else {
                                $status = '<a data-toggle="tooltip" data-placement="left" data-original-title=" In Active" class="label label-danger" style="color:#f0f0f0;" aria-describedby="tooltip659663">In Active</a>';
                            }
                            return $status;
                })
                ->escapeColumns('status')
                ->rawColumns(['action'])
                ->make(true);

        }
        
        return view('admin.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        $company->name = $request->get('name');
        $company->status  = 1;
        $company->save();

        return redirect()->route('company.index')
                         ->with('message','Insert record successfully')
                         ->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('admin.companies.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request->get('name');
        $company->status  = 1;
        $company->save();

        return redirect()->route('company.index')
                         ->with('message','Update record successfully')
                         ->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
