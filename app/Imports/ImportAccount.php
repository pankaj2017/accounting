<?php

namespace App\Imports;

use App\Models\Account;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;

//use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ImportAccount implements ToModel, WithHeadingRow, WithValidation
{
    use Importable, SkipsFailures;
    public function rules(): array
    {
        return [
            '1' => \Illuminate\Validation\Rule::unique('accounts', 'email'),
        ];
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($this->transformDate($row['finance_date']));
        return new Account([
            'name'     => $row['name'],
            'email'    => $row['email'], 
            'mobile'    => $row['mobile'], 
            'otp'    => '123456',
            'agent_name' => $row['agent_name'],
            'agent_mobile' => $row['agent_mobile'],
            'bank_name' => $row['bank_name'],
            'branch' => $row['branch'],
            'finance_rate' => $row['finance_rate'],
            'finance_duration' => $row['finance_duration'],
            'finance_date' =>  $this->transformDate($row['finance_date']),
            'type' =>  $row['type'],
            'is_verify'    => $row['is_verify'],
            'status'    => $row['status'],
        ]);
    }
    public function transformDate($value, $format = 'm-d-Y')
    {
        try {
            $car = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
            
            return \Carbon\Carbon::parse($car->toDateString())->format('m-d-Y');
        } catch (\ErrorException $e) {
            die("d");
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
