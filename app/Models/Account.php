<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{

    use SoftDeletes;
    use HasFactory;
     protected $dates = ['deleted_at'];  
    protected $fillable = ['company_id','name', 'email', 'mobile', 'otp', 'aadhaar_card', 'pan_card' ,'passport_card', 'photo', 'other', 'agent_name', 'agent_mobile', 'bank_name', 'branch', 'fund', 'charge','finance_rate', 'finance_duration', 'finance_date','type', 'is_verify', 'note', 'remind_date', 'payment_status','pay_option','revise_remind'];

    public function setFinanceDateAttribute($value) {
      $this->attributes['finance_date'] = Carbon::createFromFormat('m-d-Y',$value)->format('Y-m-d');
    }

    public function getFinanceDateAttribute($value) {
        return Carbon::parse($value)->format('m-d-Y');
    }
    public function getRemindDateAttribute($value) {
        return Carbon::parse($value)->format('m-d-Y');
    }

    public function setReviseRemindAttribute($value) {
      $this->attributes['revise_remind'] = Carbon::createFromFormat('m-d-Y',$value)->format('Y-m-d');
    }

    public function getReviseRemindAttribute($value) {
        return Carbon::parse($value)->format('m-d-Y');
    }

    /**
     * Get the company associated with the Account.
     */
    public function company()
    {
         return $this->belongsTo(Company::class, 'company_id');
    }
    /**
     * Get the agent associated with the Account.
     */
    public function agent()
    {
         return $this->belongsTo(Agent::class, 'agent_name');
    }
    /**
     * Get the bank associated with the Account.
     */
    public function bank()
    {
         return $this->belongsTo(Bank::class, 'bank_name');
    }
}
