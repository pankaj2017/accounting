<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->string('name')->nullable();
            $table->string('mobile');
            $table->string('other_mobile')->nullable();;
            $table->string('email');
            $table->string('otp');
            $table->string('aadhaar_card')->nullable();
            $table->string('pan_card')->nullable();
            $table->string('passport_card')->nullable();
            $table->string('photo')->nullable();
            $table->string('other')->nullable();
            $table->string('agent_name')->nullable();
            $table->string('agent_mobile')->nullable();
            $table->string('fund')->nullable();
            $table->string('charge')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('branch')->nullable();
            $table->timestamp('finance_date');
            $table->string('finance_rate')->nullable();
            $table->string('finance_duration')->nullable();
            $table->string('type')->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('is_verify')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
