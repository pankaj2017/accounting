@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">User</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Users </li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        	<div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Create User</h4>
                </div>
                <?php $show = false;?>
                @if(session()->has('pn'))
			    	<?php
			    	$show = true;
			    	?>
				@endif
                <div class="card-body">
                	{{ Form::open(array('route' => ('account.authentication'), 'method' => 'post','class' =>'')) }}
                	@csrf  
                	<input name="id" type="hidden" value="{{ old('id') }}" class="hiddenID">
                    <div class="row p-t-20">
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('mobile')) has-danger @endif">
				                <label class="control-label">Phone Number *</label>
				                <?=Form::text('mobile', old('mobile'), ['class' => 'form-control', 'placeholder' => 'Enter Phone Number','required'])?>
				                <span class='text-danger error'>{{ $errors->first('mobile') }}</span>
				            </div>
				        </div>
						<div class="col-md-6">
				           <div class="form-group @if($errors->has('email'))has-danger @endif">
				                <label class="control-label">Email *</label>
				                <?=Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Enter email','required'])?>
				                <span class='text-danger error'>{{ $errors->first('email') }}</span>
				            </div>
				        </div>
				        <div class="col-md-12">
				           <div class="form-group">
				                <button type="button" class="btn waves-effect waves-light btn-info get-otp" value="exit" name="save"> <i class="fa fa-check"></i> Send Otp</button>
				            </div>
				        </div>
				        <div class="col-md-6 otpbox" style="display:none;">
				           <div class="form-group @if($errors->has('otp'))has-danger @endif">
				                <label class="control-label">OTP</label>
				                <?=Form::text('otp', old('otp'), ['class' => 'form-control', 'placeholder' => 'Enter otp'])?>
				                <span class='text-danger error'>{{ $errors->first('otp') }}</span>
				            </div>
				        </div>
				    </div>
					<div class="row p-t-20">
					</div>
				    <!--/row-->
					<div class="form-actions">
					    <button type="submit" class="btn btn-success save" value="exit" name="save" disabled> <i class="fa fa-check"></i> Save</button>
					    <a data-showloading="yes" href="{{route('account.index')}}" class="btn btn-inverse">Cancel</a>
					</div>
                    <?=Form::close();?>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/dropify/dist/css/dropify.min.css')}}">
@stop

@section('script')
    <script src="{{asset('/backend/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$(document).on('click','.get-otp', function(e) {

		e.preventDefault();
   
        var mobile = $("input[name=mobile]").val();
        var email = $("input[name=email]").val();
   
        $.ajax({
            type:'POST',
       		url:"{{ route('get.otp') }}",
            data:{mobile:mobile, email:email},
            beforeSend : function() {
            	$('.preloader').css({'display': 'block', 'opacity': '0.5'});
        	},
            success:function(data){
              	if(data.success) {
              		$(".hiddenID").val(data.data.id)
              		$('.otpbox').show();
              		$('.get-otp').prop('disabled', true);
              		$('.save').prop('disabled', false);
              		alert("Sent otp successfully!");
              	}
            },
            complete: function() {
  	 			$('.preloader').css({'display': 'none', 'opacity': '1'});
    		},
        });
		
	});
	var show = "{{$show}}";
	if(show){
		$('.otpbox').show();
              		$('.get-otp').prop('disabled', true);
              		$('.save').prop('disabled', false);
	}
</script>
@include('admin.layouts.alert')
@stop
