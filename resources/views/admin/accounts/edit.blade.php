@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">User</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Users </li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        	<div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Edit User</h4>
                </div>
                <div class="card-body">
                	{!! Form::model($account, [
					   'method' => 'PATCH',
					    'route' => ['account.update', $account->id]
					,'files' => true]) !!}
                	@csrf
                	<div class="row col-md-12">
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('company_id')) has-danger @endif">
				                <label class="control-label">Select Company</label>

			                <?= Form::select('company_id', ['' =>'Select Company'] + $companies, $account->company->id ?? [], ['class' => 'form-control form-control-sm',"empty" =>"Select company"]) ?>
				                <span class='text-danger error'>{{ $errors->first('company_id') }}</span>
				            </div>
				        </div>
				        <div class="col-md-2">
				            <div class="form-group @if($errors->has('mobile')) has-danger @endif">
				                <label class="control-label">Phone Number</label>
				                <?=Form::text('mobile', old('mobile'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter Phone Number','readonly'])?>
				                <span class='text-danger error'>{{ $errors->first('mobile') }}</span>
				            </div>
				        </div>
				        <div class="col-md-4">
				            <div class="form-group @if($errors->has('other_mobile')) has-danger @endif">
				                <label class="control-label">Other Phone Number</label>
				                <?=Form::text('other_mobile', old('other_mobile'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter Other Phone Number'])?>
				                <span class='text-danger error'>{{ $errors->first('other_mobile') }}</span>
				                <span class='help-block text-muted'><small>add more using , separate</small></span>
				            </div>
				        </div>
				    </div>
                	<div class="row col-md-12">
						<div class="col-md-6">
				           <div class="form-group @if($errors->has('email'))has-danger @endif">
				                <label class="control-label">Email</label>
				                <?=Form::text('email', old('email'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter email','readonly'])?>
				                <span class='text-danger error'>{{ $errors->first('email') }}</span>
				            </div>
				        </div>
                		<div class="col-md-6">
				            <div class="form-group @if($errors->has('name')) has-danger @endif">
				                <label class="control-label">Name *</label>
				                <?=Form::text('name', old('name'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter name'])?>
				                <span class='text-danger error'>{{ $errors->first('name') }}</span>
				            </div>
				        </div>
                	</div>
                	<div class="row col-md-12">
                		<div class="col-md-6">
                			<div class="card">
	                            <div class="card-body">
	                                <h4 class="card-title">Aadhaar Upload</h4>
	                                @if(file_exists(public_path('images/'.$account->id.'/'.$account->aadhaar_card))&& !empty($account->aadhaar_card)) 
	                                <input type="file" id="input-aadhaar-card" name="aadhaar_card" class="dropify" data-default-file="{{ asset('images/'.$account->id.'/'.$account->aadhaar_card) }}"  />
									@else
	                                <input type="file" id="input-aadhaar-card" name="aadhaar_card" class="dropify" />
	                                @endif
	                                <input type="button" name="upload" value="Upload" class="form-control btn btn-info upload-image text-white mt-2">
	                        	</div>
                			</div>
                		</div>
                		<div class="col-md-6">
                			<div class="card">
	                            <div class="card-body">
	                                <h4 class="card-title">PAN Upload</h4>
	                               	@if(file_exists(public_path('images/'.$account->id.'/'.$account->pan_card))&& !empty($account->pan_card)) 
	                                <input type="file" id="input-pan-card" name="pan_card" class="dropify" data-default-file="{{ asset('images/'.$account->id.'/'.$account->pan_card) }}"/>
	                                @else
	                                <input type="file" id="input-pan-card" name="pan_card" class="dropify" />
	                                @endif
	                                <input type="button" name="upload" value="Upload" class="form-control btn btn-info upload-image_pan_card text-white mt-2">
	                            </div>
	                        </div>
                		</div>
                		<div class="col-md-6">
                			<div class="card">
	                            <div class="card-body">
	                                <h4 class="card-title">Passport Upload</h4>
	                                @if(file_exists(public_path('images/'.$account->id.'/'.$account->passport_card))&& !empty($account->passport_card)) 
	                                <input type="file" id="input-passport-card" name="passport_card" class="dropify" data-default-file="{{ asset('images/'.$account->id.'/'.$account->passport_card) }}"/>
	                                @else
	                                <input type="file" id="input-passport-card" name="passport_card" class="dropify" />
	                                @endif
	                                <input type="button" name="upload" value="Upload" class="form-control btn btn-info upload-image_passport_card text-white mt-2">
	                            </div>
	                        </div>
                		</div>
                		<div class="col-lg-6 col-md-6">
	                        <div class="card">
	                            <div class="card-body">
	                                <h4 class="card-title">Photo Upload</h4>
	                                @if(file_exists(public_path('images/'.$account->id.'/'.$account->photo))&& !empty($account->photo)) 
	                                <input type="file" id="input-photo" name="photo" class="dropify" data-default-file="{{ asset('images/'.$account->id.'/'.$account->photo) }}" />
	                                @else
	                                <input type="file" id="input-photo" name="photo" class="dropify" />
	                                @endif
	                                <input type="button" name="upload" value="Upload" class="form-control btn btn-info upload-image_photo text-white mt-2">
	                            </div>
	                        </div>
                    	</div>
                    	<div class="col-lg-6 col-md-6">
	                        <div class="card">
	                            <div class="card-body">
	                                <h4 class="card-title">Other</h4>
	                                @if(file_exists(public_path('images/'.$account->id.'/'.$account->other)) && !empty($account->other))
	                               
	                                <input type="file" id="input-other" name="other" class="dropify" data-default-file="{{ asset('images/'.$account->id.'/'.$account->other) }}" />
	                                @else
	                                <input type="file" id="input-other" name="other" class="dropify" />
	                                @endif
	                                <input type="button" name="upload" value="Upload" class="form-control btn btn-info upload-image_other text-white mt-2">
	                            </div>
	                        </div>
                    	</div>
                	</div>
                	<div class="row col-md-12">
                		<div class="col-md-6">
				            <div class="form-group @if($errors->has('agent_name')) has-danger @endif">
				            	<label class="control-label">Select Agent Name</label>
				                <?= Form::select('agent_name',['' =>'Select Agent Name'] + $agents,$account->agent_name, ['class' => 'form-control form-control-sm','id' =>'agent-id']) ?>

				                <span class='text-danger error'>{{ $errors->first('agent_name') }}</span>

				                
				            </div>
				        </div>
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('agent_mobile')) has-danger @endif">
				                <label class="control-label">Agent Mobile *</label>
				                <?=Form::text('agent_mobile', old('agent_mobile'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter agent phone','id' => 'agent_mobile','readonly'])?>
				                <span class='text-danger error'>{{ $errors->first('agent_mobile') }}</span>
				            </div>
				        </div>

                	</div>
                	<div class="row col-md-12">
                		<div class="col-md-6">
				            <div class="form-group @if($errors->has('bank_name')) has-danger @endif">
				                <label class="control-label">Bank Name *</label>
				                <?=Form::select('bank_name', ['' =>'Select Bank Name'] + $banks, $account->bank_name, ['class' => 'form-control form-control-sm', 'id' => 'bank-id'])?>
				                <span class='text-danger error'>{{ $errors->first('bank_name') }}</span>
				            </div>
				        </div>
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('branch')) has-danger @endif">
				                <label class="control-label">Branch *</label>
				                <?=Form::text('branch', old('branch'), ['class' => 'form-control form-control-sm ', 'placeholder' => 'Enter branch','id' => 'bank_branch', 'readonly'])?>
				                <span class='text-danger error'>{{ $errors->first('branch') }}</span>
				            </div>
				        </div>
                	</div>
                	<div class="row col-md-12">
                		<div class="col-md-6">
				            <div class="form-group @if($errors->has('fund')) has-danger @endif">
				                <label class="control-label">Fund *</label>
				                <?=Form::text('fund', old('fund'),['class' => 'form-control form-control-sm usd_input','placeholder' => '0.00'])?>
				                <span class='text-danger error'>{{ $errors->first('fund') }}</span>
				            </div>
				        </div>
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('charge')) has-danger @endif">
				                <label class="control-label">Charge *</label>
				                <?=Form::text('charge', old('charge'), ['class' => 'form-control form-control-sm usd_input', 'placeholder' => '0.00'])?>
				                <span class='text-danger error'>{{ $errors->first('charge') }}</span>
				            </div>
				        </div>
                	</div>
                	<div class="row col-md-12">
                		<div class="col-md-6">
				            <label class="control-label">Finance Date</label>
				            <div class="input-group @if($errors->has('finance_date')) has-danger @endif">
				                <?=Form::text('finance_date', old('finance_date'), ['class' => 'form-control form-control-sm finace_date', 'placeholder' => 'Enter Finance date'])?>
				                  <span class="input-group-addon"><i class="icon-calender"></i></span>
				                <span class='text-danger error'>{{ $errors->first('finance_date') }}</span>
				            </div>
				        </div>
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('finance_rate')) has-danger @endif">
				                <label class="control-label">Finance Rate</label>
				                <?=Form::text('finance_rate', old('finance_rate'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter rate'])?>
				                <span class='text-danger error'>{{ $errors->first('finance_rate') }}</span>
				            </div>
				        </div>
                	</div>
                	<div class="row col-md-12">
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('finance_duration')) has-danger @endif">
				                <label class="control-label">Type</label>
                                <select class="form-control form-control-sm custom-select" data-placeholder="Choose a type" tabindex="1" name="type">
                                    <option value="loan">Loan</option>
                                    <option value="single_day">Single Day</option>
                                    <option value="fd">F.D</option>
                                    <option value="1_month">1 Month</option>
                                    <option value="2_month">2 Month</option>
                                </select>
				            </div>
				        </div>
                		<div class="col-md-6">
				            <div class="form-group @if($errors->has('finance_duration')) has-danger @endif">
				                <label class="control-label">Finance Duration</label>
                                <select class="form-control form-control-sm custom-select" data-placeholder="Choose a Duration" tabindex="1" name="finance_duration">
                                    <option value="1" {{ $account->finance_duration == 1 ? 'selected' : '' }}>1 Day</option>
                                    <option value="2" {{ $account->finance_duration == 2 ? 'selected' : '' }}>2 Day</option>
                                    <option value="3" {{ $account->finance_duration == 3 ? 'selected' : '' }}>3 Day</option>
                                    <option value="5" {{ $account->finance_duration == 5 ? 'selected' : '' }}>5 Day</option>
                                    <option value="10" {{ $account->finance_duration == 10 ? 'selected' : '' }}>10 Day</option>
                                    <option value="15" {{ $account->finance_duration == 15 ? 'selected' : '' }}>15 Day</option>
                                    <option value="20" {{ $account->finance_duration == 20 ? 'selected' : '' }}>20 Day</option>
                                    <option value="30" {{ $account->finance_duration == 30 ? 'selected' : '' }}>30 Day</option>
                                    <option value="40" {{ $account->finance_duration == 40 ? 'selected' : '' }}>40 Day</option>
                                    <option value="50" {{ $account->finance_duration == 50 ? 'selected' : '' }}>50 Day</option>
                                    <option value="60" {{ $account->finance_duration == 60 ? 'selected' : '' }}>60 Day</option>
                                    <option value="90" {{ $account->finance_duration == 90 ? 'selected' : '' }}>3 Month</option>
                                    <option value="120" {{ $account->finance_duration == 120 ? 'selected' : '' }}>4 Month</option>
                                    <option value="150" {{ $account->finance_duration == 150 ? 'selected' : '' }}>5 Month</option>
                                    <option value="180" {{ $account->finance_duration == 180 ? 'selected' : '' }}>6 Month</option>
                                    <option value="365" {{ $account->finance_duration == 365 ? 'selected' : '' }}>1 year</option>
                                </select>
				                <span class='text-danger error'>{{ $errors->first('finance_duration') }}</span>
				            </div>
				        </div>
                	</div>
                	<div class="row col-md-12">
                		<div class="col-md-12">
				            <div class="form-group @if($errors->has('note')) has-danger @endif">
				            	<label class="control-label">Notes</label>
				                <?=Form::text('note', old('note'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter note'])?>
				            </div>
				        </div>
                	</div>
                	<?php
                	$pay_options = explode(",", $account->pay_option);
                	?>
                	<div class="row col-md-12">
                		<div class="col-md-6">
                			<label class="control-label">Pay Options</label><br>
                		<input type="checkbox" id="basic_checkbox_1" name="pay_option[]" value="atm" {{ in_array('atm', $pay_options) ? 'checked' : '' }}/>
                    	<label for="basic_checkbox_1">ATM</label> &nbsp;&nbsp;

                    	<input type="checkbox" id="basic_checkbox_2" name="pay_option[]" value="passbook" {{ in_array('passbook', $pay_options) ? 'checked' : '' }}/>
                    	<label for="basic_checkbox_2">Passbook</label> &nbsp;&nbsp;

                    	<input type="checkbox" id="basic_checkbox_3" name="pay_option[]" value="sign" {{ in_array('sign', $pay_options) ? 'checked' : '' }}/>
                    	<label for="basic_checkbox_3">Sign</label> &nbsp;&nbsp;
                    </div>
                    </div>
                	<div class="form-actions p-20">
					    <button type="submit" class="btn btn-success" value="exit" name="save"> <i class="fa fa-check"></i> Save</button>
					    <a data-showloading="yes" href="{{route('account.index')}}" class="btn btn-inverse">Cancel</a>
					   
					</div>
                    <?=Form::close();?>
                </div>
            </div>
        	
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/dropify/dist/css/dropify.min.css')}}">    
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
  
@stop

@section('script')
     <script src="{{asset('/backend/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <script src="{{asset('/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>


<script type="text/javascript">
	$(document).ready(function() {
        // Basic
        var drEvent = $('.dropify').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
        	console.log("Server Remove Progress");
        });
    });
	$(document).on('click','.get-otp', function(e) {
		alert("Sent otp successfully!");
	});

	$('.finace_date').datepicker({
        format: 'mm-dd-yyyy',
        // container: container,
        todayHighlight: true,
        autoclose: true,
        // endDate: "+100y",
        // startDate: '0'
	});

	$('.upload-image').on('click', function (ev) {
		ev.preventDefault();
		var postData= new FormData();
		var id = "{{$account->id}}";
		// var data = new FormData();
		postData.append('aadhaar_card', $('#input-aadhaar-card').prop('files')[0]);
		postData.append('_token', "{{ csrf_token() }}");
		postData.append('id', id);
        // postData.append('file',$('#input-aadhaar-card').get(0).files);
	    $.ajax({
	      url: "{{route('cropimage')}}",
	      type: "POST",
	      data: postData,
	      processData : false,
        contentType : false,
        mimeType: 'multipart/form-data',
        beforeSend: function () {
        	$('.upload-image').css({'opacity': '0.5'});
    	},
	    success: function (data) {
	      	// location.reload(true);
	        // html = '<img src="' + img + '" />';
	        // $("#preview-crop-image").html(html);
	    },
	    complete: function(){
	     // Handle the complete event
	    	$('.upload-image').css({'opacity': '1'});
	  	}
	    });
	});
	$('.upload-image_pan_card').on('click', function (ev) {
		ev.preventDefault();
		var postData= new FormData();
		var id = "{{$account->id}}";
		// var data = new FormData();
		postData.append('pan_card', $('#input-pan-card').prop('files')[0]);
		postData.append('_token', "{{ csrf_token() }}");
		postData.append('id', id);
	    $.ajax({
	      	url: "{{route('cropimage')}}",
	      	type: "POST",
	      	data: postData,
	      	processData : false,
        	contentType : false,
        	mimeType: 'multipart/form-data',
        	beforeSend: function () {
        		$('.upload-image_pan_card').css({'opacity': '0.5'});
    		},
	      	success: function (data) {
		      	// location.reload(true);
		        // html = '<img src="' + img + '" />';
		        // $("#preview-crop-image").html(html);
	      	},
	      	complete: function(){
	     		// Handle the complete event
		    	$('.upload-image_pan_card').css({'opacity': '1'});
		  	}
	    });
	});
	$('.upload-image_passport_card').on('click', function (ev) {
		ev.preventDefault();
		var postData= new FormData();
		var id = "{{$account->id}}";
		// var data = new FormData();
		postData.append('passport_card', $('#input-passport-card').prop('files')[0]);
		postData.append('_token', "{{ csrf_token() }}");
		postData.append('id', id);
	    $.ajax({
	      	url: "{{route('cropimage')}}",
	      	type: "POST",
	      	data: postData,
	      	processData : false,
        	contentType : false,
        	mimeType: 'multipart/form-data',
        	beforeSend: function () {
        		$('.upload-image_passport_card').css({'opacity': '0.5'});
    		},
	      	success: function (data) {
		      	// location.reload(true);
		        // html = '<img src="' + img + '" />';
		        // $("#preview-crop-image").html(html);
	      	},
	      	complete: function(){
	     		// Handle the complete event
		    	$('.upload-image_passport_card').css({'opacity': '1'});
		  	}
	    });
	});
	$('.upload-image_photo').on('click', function (ev) {
		ev.preventDefault();
		var postData= new FormData();
		var id = "{{$account->id}}";
		// var data = new FormData();
		postData.append('photo', $('#input-photo').prop('files')[0]);
		postData.append('_token', "{{ csrf_token() }}");
		postData.append('id', id);
	    $.ajax({
	      	url: "{{route('cropimage')}}",
	      	type: "POST",
	      	data: postData,
	      	processData : false,
        	contentType : false,
        	mimeType: 'multipart/form-data',
        	beforeSend: function () {
        		$('.upload-image_photo').css({'opacity': '0.5'});
    		},
	      	success: function (data) {
		      	// location.reload(true);
		        // html = '<img src="' + img + '" />';
		        // $("#preview-crop-image").html(html);
	      	},
	      	complete: function(){
	     		// Handle the complete event
		    	$('.upload-image_photo').css({'opacity': '1'});
		  	}
	    });
	});
	$('.upload-image_other').on('click', function (ev) {
		ev.preventDefault();
		var postData= new FormData();
		var id = "{{$account->id}}";
		// var data = new FormData();
		postData.append('other', $('#input-other').prop('files')[0]);
		postData.append('_token', "{{ csrf_token() }}");
		postData.append('id', id);
	    $.ajax({
	      	url: "{{route('cropimage')}}",
	      	type: "POST",
	      	data: postData,
	      	processData : false,
        	contentType : false,
        	mimeType: 'multipart/form-data',
        	beforeSend: function () {
        		$('.upload-image_other').css({'opacity': '0.5'});
    		},
	      	success: function (data) {
		      	// location.reload(true);
		        // html = '<img src="' + img + '" />';
		        // $("#preview-crop-image").html(html);
	      	},
	      	complete: function(){
	     		// Handle the complete event
		    	$('.upload-image_other').css({'opacity': '1'});
		  	}
	    });
	});

</script>
<script>
jQuery(document).ready(function(){
// jQuery('#ajaxSubmit').click(function(e){
 	getPhoneByAgent($("#agent-id" ).val());
 	jQuery("#agent-id").change(function(e){
 		var agentId = this.value;
 		getPhoneByAgent(agentId);
   	});
});
function getPhoneByAgent(agentId) {

	// e.preventDefault();
   	$.ajaxSetup({
      	headers: {
          	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      	}
  	});
	jQuery.ajax({
	  	url: "{{ url('admin/get-agent-phone') }}",
	  	method: 'post',
	  	data: {
	    	agent_id: agentId
	  	},
	  	success: function(result){
	  		$('#agent_mobile').val(result.data.mobile);
	     	// console.log();
	  	}
  	});
}
jQuery(document).ready(function(){
// jQuery('#ajaxSubmit').click(function(e){
   	getBanknameById($("#bank-id").val());
 	jQuery("#bank-id").change(function(e){
 		var bankId = this.value;
   		e.preventDefault();
   		getBanknameById(bankId);
   	});
});
$(document).ready(function () {
  $('.usd_input').mask('000,000,000,000.00', { reverse: true });
});

function getBanknameById(bankId) {
   	$.ajaxSetup({
      	headers: {
          	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      	}
  	});
	jQuery.ajax({
  		url: "{{ url('admin/get-bank-name') }}",
  		method: 'post',
  		data: {
    		bank_id: bankId
  		},
  		success: function(result){
  			$('#bank_branch').val(result.data.branch);
     		// console.log();
  		}
  	});
}
</script>
@include('admin.layouts.alert')
@stop
