@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Import</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Users </li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        	<div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Import User</h4>
                </div>
                <?php $show = false;?>
                @if(session()->has('pn'))
			    	<?php
			    	$show = true;
			    	?>
				@endif
                <div class="card-body">
                	{{ Form::open(array('route' => ('import'), 'method' => 'post','class' =>'','files' => true)) }}
                	@csrf  
                    <div class="row p-t-20">
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('file')) has-danger @endif">
				                <label class="control-label">Select Excel file </label>
				                <?=Form::file('file', old('file'), ['class' => 'form-control', 'placeholder' => 'Enter Phone Number','required'])?>
				                <span class='text-danger error'>{{ $errors->first('file') }}</span>
				            </div>
				        </div>
				    </div>
					<div class="row p-t-20">
					</div>
				    <!--/row-->
					<div class="form-actions">
					    <button type="submit" class="btn btn-success" value="exit" name="save"> <i class="fa fa-check"></i> Save</button>
					    <a data-showloading="yes" href="{{route('account.index')}}" class="btn btn-inverse">Cancel</a>
					</div>
                    <?=Form::close();?>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/dropify/dist/css/dropify.min.css')}}">
@stop

@section('script')
    <script src="{{asset('/backend/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

@include('admin.layouts.alert')
@stop
