@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Account</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">list</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                        <div class="text-right">
                            <a href="{{route('export')}}" class="btn waves-effect waves-light btn-info" title="Add New">Export Excel</a>
                            <a href="{{route('import-view')}}" class="btn waves-effect waves-light btn-info" title="Add New">Import Excel</a>
                             <a href="{{route('account.create')}}" class="btn waves-effect waves-light btn-info" title="Add New">Add New</a>
                        </div>
                        <div class="table-responsive m-t-20">
                        <table id="master_table" class="table table-bordered table-striped data-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Agent</th>
                                    <th>Verify</th>
                                    <th>Status</th>
                                    <th>Payment</th>
                                    <th>Created</th>
                                    <th width="100px">Action</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modal')
    <div class="modal fade" id="editPeopleProPreferencesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <div class="modal fade" id="editPaymentReminderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>
@stop
@section('script')
<script src="{{asset('/backend/js/plugins/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/backend/js/plugins/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/js/delete_script.js')}}"></script>
<script src="{{asset('backend/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('backend/js/fnStandingRedraw.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- public/backend/plugins/sweetalert -->
<script type="text/javascript">
$(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('account.index') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'agent_name', name: 'agent_name'},
            {data: 'verify', name: 'verify'},
            {data: 'status', name: 'status'},
            {data: 'payment_status', name: 'payment_status'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });

$(document).on("click", ".update_payment_status", function(e){
        $('#editPeopleProPreferencesModal .modal-content').html(""); 
        acc_id = $(this).data('account_id');
        $.ajax({
            method: "GET",
            headers: {
                Accept: "application/json"
            },
            url: "{{ route('payment.status.get') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "account_id": acc_id
            },
            success:  function(result) { 
                if (result.html) {
                    $('#editPeopleProPreferencesModal .modal-content').html(result.html);
                    $('#editPeopleProPreferencesModal').modal('show');
                } 
            }
        })
    });
    $(document).on('submit', '#update_preferences_people_post', function(e){
        e.preventDefault();
        let formData = $(this).serializeArray();
        $.ajax({
            method: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{ route('payment.status.post') }}",
            data: formData,
            success:  function(result) { 
                if (result.success) {
                    $('#editPaymentReminderModal').modal('hide');
                    window.location.reload();
                } 
            }
        })
    });

    $(document).on("click", ".revise_payment_notify", function(e){
        $('#editPaymentReminderModal .modal-content').html(""); 
        acc_id = $(this).data('account_id');
        $.ajax({
            method: "GET",
            headers: {
                Accept: "application/json"
            },
            url: "{{ route('revise.paymentnotify.get') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "account_id": acc_id
            },
            success:  function(result) { 
                if (result.html) {
                    $('#editPaymentReminderModal .modal-content').html(result.html);
                    $('#editPaymentReminderModal').modal('show');
                } 
            }
        })
    });
    $(document).on('submit', '#revise_payment_post', function(e){
        e.preventDefault();
        let formData = $(this).serializeArray();
        $.ajax({
            method: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{ route('revise.payment.notify.post') }}",
            data: formData,
            success:  function(result) { 
                if (result.success) {
                    $('#editPaymentReminderModal').modal('hide');
                    window.location.reload();
                } 
            }
        })
    });


    function deleteacc(id) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
                method: "delete",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('account.delete') }}",
                data: {id:id},
                success:  function(result) { 
                    if (result.success) {
                        $('#editPaymentReminderModal').modal('hide');
                        window.location.reload();
                    } 
                }
            })
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          }
        })
    }

</script>
@include('admin.layouts.alert')
@stop

<!-- 
<!DOCTYPE html>
<html>
<head>
    <title>Laravel 8 Datatables Tutorial - ItSolutionStuff.com</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
    
<div class="container">
    <h1>Laravel 8 Datatables Tutorial <br/> ItSolutionStuff.com</h1>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
   
</body>
   
<script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('users.index') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>
</html> -->