<form action="#" method="post" id="revise_payment_post">
    @csrf
    <div class="modal-body p-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <button type="button" class="close py-1 px-2 mr-3 mt-4 editFirstModal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="px-4 py-2 border-bottom"><h2 class="my-3">Update Payment Remind</h2></div>
                <div class="py-4" style="max-height: calc(100vh - 250px);overflow-y: auto;overflow-x: hidden;">
                    {{ Form::hidden('account_id', $accountId) }}
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <div class="row">
                                <!-- <div class="col-md-6"> -->
						            <label class="control-label">Select Revise Remind Date</label>
						            <div class="input-group @if($errors->has('revise_remind')) has-danger @endif">
						                <?=Form::text('revise_remind', old('revise_remind',$revise_remind), ['class' => 'form-control form-control-sm revise_date', 'placeholder' => 'Enter revise remind date'])?>
						                  <span class="input-group-addon"><i class="icon-calender"></i></span>
						                <span class='text-danger error'>{{ $errors->first('revise_remind') }}</span>
						            </div>
						        <!-- </div> -->
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="pt-4 border-top">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-block rounded-pill btn-primary-blue py-2 mb-4 font-18">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal-footer d-none">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
</div>

    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <script src="{{asset('/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.revise_date').datepicker({
        format: 'mm-dd-yyyy',
        // container: container,
        todayHighlight: true,
        autoclose: true,
        // endDate: "+100y",
        // startDate: '0'
	});

	});
</script>