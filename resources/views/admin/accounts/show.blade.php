@extends('admin.layouts.layout')
@section('content')
<?php
$ext = '';
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Account</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Users </li>
            <li class="breadcrumb-item active">Show</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Details</h4>
                </div>
                <div class="card-body">
                	 <div class="text-right">
                            <a href="{{route('downloadPDF',$account->id)}}" class="btn waves-effect waves-light btn-info" title="Add New">Export PDF</a>
                        </div>
					<div class="row">
                        <div class="col-md-3 col-xs-6"> <strong>Company</strong>
                            <br>
                            <p class="text-muted">{{$account->company->name ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                            <br>
                            <p class="text-muted">{{$account->name?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                            <br>
                            <p class="text-muted">{{$account->mobile ?? "-"}}</p>
                            <p class="text-muted">{{$account->other_mobile ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                            <br>
                            <p class="text-muted">{{$account->email ?? "-"}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Agent Name</strong>
                            <br>
                            <p class="text-muted">{{$account->agent->name ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Agent Mobile</strong>
                            <br>
                            <p class="text-muted">{{$account->agent->mobile ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Bank name</strong>
                            <br>
                            <p class="text-muted">{{$account->agent->mobile ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Branch</strong>
                            <br>
                            <p class="text-muted">{{$account->branch ?? "-"}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Finance Date</strong>
                            <br>
                            <p class="text-muted">{{$account->finance_date ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Finance Rate</strong>
                            <br>
                            <p class="text-muted">{{$account->finance_date ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Bank name</strong>
                            <br>
                            <p class="text-muted">{{$account->bank_name ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Branch</strong>
                            <br>
                            <p class="text-muted">{{$account->branch ?? "-"}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Fund</strong>
                            <br>
                            <p class="text-muted"> &#x20b9; {{$account->fund ?? "-"}}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Charge</strong>
                            <br>
                            <p class="text-muted"> &#x20b9; {{$account->charge ?? "-"}}</p>
                        </div>
                        <div class="col-md-6 col-xs-6 b-r"> <strong>Note</strong>
                            <br>
                            <p class="text-muted"> {{$account->note ?? "-"}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row el-element-overlay">
	                    <div class="col-md-12"> </div>
		                    <div class="col-lg-3 col-md-6">
		                        <div class="card">
		                            <div class="el-card-item">
		                                <div class="el-card-avatar el-overlay-1">
		                                	

		                                	@if(file_exists(public_path('images/'.$account->id.'/'.$account->aadhaar_card))&& !empty($account->aadhaar_card))
		                                	<?php
		                                	$ext = pathinfo(public_path('images/'.$account->id.'/'.$account->aadhaar_card), PATHINFO_EXTENSION);
                                			
                                			?>
	                                			@if($ext == "pdf")
	                                				<a href="{{ asset('images/'.$account->id.'/'.$account->aadhaar_card) }}" target="_blank">
													     <button class="btn"><i class="fa fa-download"></i> Download File</button>
													 </a>
			                                	@else
			                                		<img src="{{ asset('images/'.$account->id.'/'.$account->aadhaar_card) }}" alt="user" />
			                                	@endif
		                                	@else
		                                	<img src="https://via.placeholder.com/600x600?text=Not+Found" alt="user" />
		                                	@endif  
		                                	@if($ext != "pdf")
		                                    <div class="el-overlay">
		                                        <ul class="el-info">
		                                            <li>
		                                            	@if(file_exists(public_path('images/'.$account->id.'/'.$account->aadhaar_card))&& !empty($account->aadhaar_card))
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset('images/'.$account->id.'/'.$account->aadhaar_card) }}"><i class="icon-magnifier"></i>
		                                            	</a>
		                                            	@else
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="https://via.placeholder.com/600x600?text=Not+Found"><i class="icon-magnifier"></i></a>
		                                            	@endif
		                                            </li>
		                                        </ul>
		                                    </div>
		                                    @endif
		                                </div>
		                                <div class="el-card-content">
		                                    <h3 class="box-title">Aadhaar card</h3>
		                                    <br/> 
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-lg-3 col-md-6">
		                        <div class="card">
		                            <div class="el-card-item">
		                                <div class="el-card-avatar el-overlay-1">
		                                	@if(file_exists(public_path('images/'.$account->id.'/'.$account->pan_card))&& !empty($account->pan_card))
			                                	<?php
			                                	$ext = pathinfo(public_path('images/'.$account->id.'/'.$account->pan_card), PATHINFO_EXTENSION);
	                                			?>
                                				@if($ext == "pdf")
	                                				<a href="{{ asset('images/'.$account->id.'/'.$account->pan_card) }}" target="_blank">
													    <button class="btn"><i class="fa fa-download"></i> Download File</button>
													 </a>
			                                	@else 
		                                		<img src="{{ asset('images/'.$account->id.'/'.$account->pan_card) }}" alt="user" />
		                                		@endif
		                                	@else
		                                	<img src="https://via.placeholder.com/600x600?text=Not+Found" alt="user" />
		                                	@endif
		                                	@if($ext != "pdf")
		                                    <div class="el-overlay">
		                                        <ul class="el-info">
		                                            <li>
		                                            	@if(file_exists(public_path('images/'.$account->id.'/'.$account->pan_card))&& !empty($account->pan_card))
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset('images/'.$account->id.'/'.$account->pan_card) }}" target="_blank"><i class="icon-magnifier"></i></a>
		                                            	@else
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="https://via.placeholder.com/600x600?text=Not+Found" target="_blank"><i class="icon-magnifier"></i></a>
		                                            	@endif
		                                            </li>
		                                        </ul>
		                                    </div>
		                                    @endif
		                                </div>
		                                <div class="el-card-content">
		                                    <h3 class="box-title">PAN card</h3>
		                                    <br/> 
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-lg-3 col-md-6">
		                        <div class="card">
		                            <div class="el-card-item">
		                                <div class="el-card-avatar el-overlay-1">
		                                	@if(file_exists(public_path('images/'.$account->id.'/'.$account->passport_card))&& !empty($account->passport_card))
		                                	<?php
		                                	$ext = pathinfo(public_path('images/'.$account->id.'/'.$account->passport_card), PATHINFO_EXTENSION);
                                			
                                			?>
                                				@if($ext == "pdf")
	                                				<a href="{{ asset('images/'.$account->id.'/'.$account->passport_card) }}" target="_blank">
													    <button class="btn"><i class="fa fa-download"></i> Download File</button>
													 </a>
			                                	@else
		                                			<img src="{{ asset('images/'.$account->id.'/'.$account->passport_card) }}" alt="user" />
		                                		@endif
		                                	@else
		                                	<img src="https://via.placeholder.com/600x600?text=Not+Found" alt="user" />
		                                	@endif
		                                	@if($ext != "pdf")
		                                    <div class="el-overlay">
		                                        <ul class="el-info">
		                                            <li>
		                                            	@if(file_exists(public_path('images/'.$account->id.'/'.$account->passport_card))&& !empty($account->passport_card)) 
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset('images/'.$account->id.'/'.$account->passport_card) }}" target="_blank"><i class="icon-magnifier"></i></a>
		                                            	@else
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="https://via.placeholder.com/600x600?text=Not+Found" target="_blank"><i class="icon-magnifier"></i></a>
		                                            	@endif
		                                            </li>
		                                        </ul>
		                                    </div>
		                                    @endif
		                                </div>
		                                <div class="el-card-content">
		                                    <h3 class="box-title">Passport</h3>
		                                    <br/> 
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-lg-3 col-md-6">
		                        <div class="card">
		                            <div class="el-card-item">
		                                <div class="el-card-avatar el-overlay-1">
		                                 	@if(file_exists(public_path('images/'.$account->id.'/'.$account->photo))&& !empty($account->photo))
		                                 	<?php
		                                	$ext = pathinfo(public_path('images/'.$account->id.'/'.$account->photo), PATHINFO_EXTENSION);
                                			
                                			?>
                                				@if($ext == "pdf")
	                                				<a href="{{ asset('images/'.$account->id.'/'.$account->photo) }}" target="_blank">
													    <button class="btn"><i class="fa fa-download"></i> Download File</button>
													 </a>
			                                	@else 
		                                		<img src="{{ asset('images/'.$account->id.'/'.$account->photo) }}" alt="user" />
		                                		@endif
		                                	@else
		                                	<img src="https://via.placeholder.com/600x600?text=Not+Found" alt="user" />
		                                	@endif
		                                	@if($ext != "pdf")
		                                    <div class="el-overlay">
		                                        <ul class="el-info">
		                                            <li>
		                                            	 @if(file_exists(public_path('images/'.$account->id.'/'.$account->photo))&& !empty($account->photo))
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset('images/'.$account->id.'/'.$account->photo) }}" target="_blank"><i class="icon-magnifier"></i></a></li>
		                                            	@else
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="https://via.placeholder.com/600x600?text=Not+Found" target="_blank"><i class="icon-magnifier"></i></a></li>
		                                            	@endif
		                                        </ul>
		                                    </div>
		                                	@endif
		                                </div>
		                                <div class="el-card-content">
		                                    <h3 class="box-title">Photo</h3>
		                                    <br/> 
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-lg-3 col-md-6">
		                        <div class="card">
		                            <div class="el-card-item">
		                                <div class="el-card-avatar el-overlay-1">
		                                   	@if(file_exists(public_path('images/'.$account->id.'/'.$account->other))&& !empty($account->other))  
		                                   		<?php
			                                	$ext = pathinfo(public_path('images/'.$account->id.'/'.$account->other), PATHINFO_EXTENSION);
	                                			?>
                                				@if($ext == "pdf")
	                                				<a href="{{ asset('images/'.$account->id.'/'.$account->other) }}" target="_blank">
													    <button class="btn"><i class="fa fa-download"></i> Download File</button>
													 </a>
			                                	@else 
		                                		<img src="{{ asset('images/'.$account->id.'/'.$account->other) }}" alt="user" />
		                                		@endif
		                                	@else
		                                	<img src="https://via.placeholder.com/600x600?text=Not+Found" alt="user" />
		                                	@endif
		                                	@if($ext != "pdf")
		                                    <div class="el-overlay">
		                                        <ul class="el-info">
		                                            <li>
		                                            	@if(file_exists(public_path('images/'.$account->id.'/'.$account->other))&& !empty($account->other)) 
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset('images/'.$account->id.'/'.$account->other) }}" target="_blank"><i class="icon-magnifier"></i></a>
		                                            	@else
		                                            	<a class="btn default btn-outline image-popup-vertical-fit" href="https://via.placeholder.com/600x600?text=Not+Found" target="_blank"><i class="icon-magnifier"></i></a>
		                                            	@endif
		                                            </li>
		                                        </ul>
		                                    </div>
		                                    @endif
		                                </div>
		                                <div class="el-card-content">
		                                    <h3 class="box-title">Other</h3>
		                                    <br/> 
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
</div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/dropify/dist/css/dropify.min.css')}}">
@stop

@section('script')
    <script src="{{asset('/backend/plugins/dropify/dist/js/dropify.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function() {
        // Basic
        $('.dropify').dropify();
    });
	$(document).on('click','.get-otp', function(e) {
		alert("Sent otp successfully!");
	});
</script>
@include('admin.layouts.alert')
@stop
