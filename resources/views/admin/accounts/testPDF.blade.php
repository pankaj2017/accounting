<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 0px solid #dddddd;
  text-align: left;
  padding: 8px;
}

/*tr:nth-child(even) {
  background-color: #dddddd;
}*/
</style>
</head>
<body>

<h2> <img src="{{ public_path('backend/images/big1.jpg') }}" style="width: 20%"> </h2>

<table>
  <!-- <tr>
    <th>Company</th>
    <th>Contact</th>
    <th>Country</th>
  </tr> -->
  <tr>
    <td><b>Company</b></td>
    <td><b>Full Name</b></td>
    <td><b>Mobile</b></td>
    <td><b>Email</b></td>
  </tr>
  <tr>
    <td>{{$company_name}}</td>
    <td>{{$name}}</td>
    <td>{{$mobile}}</td>
    <td>{{$email}}</td>
  </tr>
  <tr>
    <td><b>Agent Name</b></td>
    <td><b>Agent Mobile</b></td>
    <td><b>Bank Name</b></td>
    <td><b>Branch Name</b></td>
  </tr>
  <tr>
    <td>{{$agent_name}}</td>
    <td>{{$agent_mobile}}</td>
    <td>{{$bank_name}}</td>
    <td>{{$branch}}</td>
  </tr>
  <tr>
    <td><b>Finance Date</b></td>
    <td><b>Finance Rate</b></td>
    <td><b>Fund</b></td>
    <td><b>Charge</b></td>
  </tr>
  <tr>
    <td>{{$finance_date}}</td>
    <td>{{$finance_rate}}</td>
    <td>{{$fund}}</td>
    <td>{{$charge}}</td>
  </tr>
  <tr>
    <td colspan="2"><b>Note</b></td>
  </tr>
  <tr>
    <td colspan="2">{{$note}}</td>
  </tr>
  <tr>
    <td colspan="2"><b>Aadhaar Card</b></td>
    <td colspan="2"><b>Pan Card</b></td>
  </tr>
  <tr>
    <td colspan="2">
      @if(isset($aadhaar_card))
      <img src="{{ public_path('images/'.$id.'/'.$aadhaar_card) }}" style="width: 100%; height:200px;" >
      @else
      <span>-</span>
      @endif
    </td>
    <td colspan="2">
      @if(isset($pan_card))
      <img src="{{ public_path('images/'.$id.'/'.$pan_card) }}" style="width: 100%; height:200px;">
      @else
      <span>-</span>
      @endif
    </td>
  </tr>
  <tr>
    <td colspan="2"><b>Passport</b></td>
    <td colspan="2"><b>Photo</b></td>
  </tr>
  <tr>
    <td colspan="2">
      @if(isset($passport_card))
      <img src="{{ public_path('images/'.$id.'/'.$passport_card) }}" style="width: 100%; height:200px;">
      @else
      <span>-</span>
      @endif
    </td>
    <td colspan="2">
      @if(isset($photo))
      <img src="{{ public_path('images/'.$id.'/'.$photo) }}" style="width: 100%; height:200px;">
       @else
      <span>-</span>
      @endif
    </td>
  </tr>
  <tr>
    <td colspan="2"><b>Other</b></td>
  </tr>
  <td colspan="2">
    @if(isset($other))
    <img src="{{ public_path('images/'.$id.'/'.$other) }}" style="width: 100%; height:200px;">
     @else
    <span>-</span>
    @endif
  </td>
</table>

</body>
</html>

