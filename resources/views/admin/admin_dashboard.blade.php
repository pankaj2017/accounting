@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Dashboard</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <div class="card-group">
                   <!--  <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                                    <h3 class="">2456</h3>
                                    <h6 class="card-subtitle">New Projects</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                    <!-- <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                                    <h3 class="">546</h3>
                                    <h6 class="card-subtitle">Pending Project</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                   <!--  <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                                    <h3 class="">$24561</h3>
                                    <h6 class="card-subtitle">Total Cost</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                    <!-- <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                                    <h3 class="">$30010</h3>
                                    <h6 class="card-subtitle">Total Earnings</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="bg-light p-20">
                                <div class="d-flex">
                                    <div class="align-self-center">
                                        <h3 class="m-b-0">Reminder</h3></div>
                                    <div class="ml-auto align-self-center">
                                        <!-- <h2 class="text-success">$5470</h2></div> -->
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table color-table primary-table color-bordered-table info-bordered-table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Agnet Name/mobile</th>
                                                <th>Fund</th>
                                                <th>Charge</th>
                                                <th>Action</th>
                                                <th>Payment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($accounts as $account)
                                            <tr>
                                                <td>{{$account->name}}</td>
                                                <td>{{$account->mobile}}</td>
                                                <td>{{$account->agent->name}} <br>{{$account->agent->mobile}}</td>
                                                <td> &#x20b9; {{$account->fund}}</td>
                                                <td> &#x20b9; {{$account->charge}}</td>
                                                <td> <a href="{{route('account.show', $account->id) }}" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a><br>
                                                    <a href="#" data-acc_id="{{$account->id}}" class="reminder" data-toggle="tooltip" data-original-title="Reminder"> <i class="fa fa-share text-inverse m-r-10"></i>Reminder </a><br>
                                                    <a href="#" data-acc_re_id="{{$account->id}}" class="re_reminder" data-toggle="tooltip" data-original-title="Re-reminder"> <i class="fa fa-share text-inverse m-r-10"></i>Re-reminder </a>
                                                </td>
                                                <td> <a href="#" data-account_id="{{$account->id}}" class="update_payment_status" data-toggle="tooltip" data-original-title="Update Payment Status"> <i class="fa fa-cog text-inverse m-r-10"></i></a> 
                                                    @if($account->payment_status == 0) 
                                                    <span class="label label-warning">Un Paid</span>

                                                    @elseif($account->payment_status == 1)
                                                    <span class="label label-success">Paid</span>
                                                    @elseif($account->payment_status == 2)
                                                    <span class="label label-info">By Pass</span>
                                                    @endif

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-group" style="display:none">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-blogger text-info"></i></h2>
                        <h3 class="">4</h3>
                        <h6 class="card-subtitle"><a href="">Blog</a></h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card" style="display:none">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-calendar-question text-success"></i></h2>
                        <h3 class=""></h3>
                        <h6 class="card-subtitle"><a href="">Faqs</a></h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card" style="display:none">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-settings-box text-purple"></i></h2>
                        <!-- <h3 class="">$24561</h3> -->
                        <h6 class="card-subtitle"><a href="">Configration</a></h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <!-- <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                        <h3 class="">$30010</h3>
                        <h6 class="card-subtitle">Total Earnings</h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    
</div>

@stop
@section('modal')
    <div class="modal fade" id="editPeopleProPreferencesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>
@stop
@section('script')
   <!-- <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="{{asset('/backend/js/plugins/jquery.dataTables.min.js')}}"></script>

<script>

    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                
               
            });
            
        });
    });
    </script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click','.reminder', function(e) {
        e.preventDefault();
        var account_id = $(this).data("acc_id")
        $.ajax({
            type:'POST',
            url:"{{ route('reminder') }}",
            data:{account_id:account_id},
            beforeSend : function() {
                $('.preloader').css({'display': 'block', 'opacity': '0.5'});
            },
            success:function(data) {
                if(data.success) {
                    alert("Send reminder message successfully");
                }
            },
            complete: function() {
                $('.preloader').css({'display': 'none', 'opacity': '1'});
            },
        });
    });
    $(document).on('click','.re_reminder', function(e) {
        e.preventDefault();
        var account_id = $(this).data('acc_re_id');

        $.ajax({
            type:'POST',
            url:"{{ route('re.reminder') }}",
            data:{account_id:account_id},
            beforeSend : function() {
                $('.preloader').css({'display': 'block', 'opacity': '0.5'});
            },
            success:function(data) {
                if(data.success) {
                    alert("Send Re-reminder message successfully");
                }
            },
            complete: function() {
                $('.preloader').css({'display': 'none', 'opacity': '1'});
            },
        });
    });
    $(document).on("click", ".update_payment_status", function(e){
        $('#editPeopleProPreferencesModal .modal-content').html(""); 
        acc_id = $(this).data('account_id');
        $.ajax({
            method: "GET",
            headers: {
                Accept: "application/json"
            },
            url: "{{ route('payment.status.get') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "account_id": acc_id
            },
            success:  function(result) { 
                if (result.html) {
                    $('#editPeopleProPreferencesModal .modal-content').html(result.html);
                    $('#editPeopleProPreferencesModal').modal('show');
                } 
            }
        })
    });
    $(document).on('submit', '#update_preferences_people_post', function(e){
        e.preventDefault();
        let formData = $(this).serializeArray();
        $.ajax({
            method: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{ route('payment.status.post') }}",
            data: formData,
            success:  function(result) { 
                if (result.success) {
                    $('#editPeopleProPreferencesModal').modal('hide');
                    window.location.reload();
                } 
            }
        })
    });

</script>
@stop
