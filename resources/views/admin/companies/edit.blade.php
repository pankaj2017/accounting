@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Company</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Company </li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        	<div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Edit Company</h4>
                </div>
                <div class="card-body">
                	{!! Form::model($company, [
					   'method' => 'PATCH',
					    'route' => ['company.update', $company->id]
					,'files' => true]) !!}
                	@csrf
                	<div class="row col-md-12">
				        <div class="col-md-6">
				            <div class="form-group @if($errors->has('name')) has-danger @endif">
				                <label class="control-label">Company Name</label>
				                <?=Form::text('name', old('name'), ['class' => 'form-control form-control-sm', 'placeholder' => 'Enter Company Name'])?>
				                <span class='text-danger error'>{{ $errors->first('name') }}</span>
				            </div>
				        </div>
				    </div>
                	<div class="form-actions p-20">
					    <button type="submit" class="btn btn-success" value="exit" name="save"> <i class="fa fa-check"></i> Save</button>
					    <a data-showloading="yes" href="{{route('company.index')}}" class="btn btn-inverse">Cancel</a>
					   
					</div>
                    <?=Form::close();?>
                </div>
            </div>
        	
        </div>
    </div>
</div>
@stop

@section('css')
  
@stop

@section('script')
   
@include('admin.layouts.alert')
@stop
