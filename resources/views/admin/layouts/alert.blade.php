@if(Session::has('message'))
	@if(Session::get('message_type'))
		<script>
	        $(document).ready(function(){
		        toastr.{{ Session::get('message_type') }}
		        ('{{ Session::get('message') }}');
	        });
	    </script>
    @endif
@endif

@if ($errors->any())

<script type="text/javascript">
	$(document).ready(function(){
        toastr.error
        ("<b>There were some errors.</b>");
	});
</script>
@endif



