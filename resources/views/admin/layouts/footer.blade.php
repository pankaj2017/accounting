    
    <!-- ============================================================== -->
    <script src="{{asset('/backend/js/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('/backend/js/bootstrap/popper.min.js')}}"></script>

    <script src="{{asset('/backend/js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('/backend/js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('/backend/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('/backend/js/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{asset('/backend/js/plugins/sticky-kit.min.js')}}"></script>
    <script src="{{asset('/backend/js/plugins/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('/backend/js/custom.min.js')}}"></script>
    <script src="{{asset('/backend/js/toastr.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
   
    <script src="{{asset('/backend/js/plugins/jQuery.style.switcher.js')}}"></script>
    <script src="{{asset('/backend/plugins/toast-master/js/toastr.min.js')}}"></script>
    <script src="{{asset('/backend/js/plugins/dashboard1.js')}}"></script>
    <script src="{{asset('/backend/js/plugins/morris.min.js')}}"></script>
    <script src="{{asset('/backend/js/plugins/raphael-min.js')}}"></script>