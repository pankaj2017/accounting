<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <!-- Logo icon --><b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="{{URL::asset('/backend/images/small.jpg')}}" alt="mkvfinance" class="dark-logo" />
                    <!-- Light Logo icon -->
                    <img src="{{URL::asset('/backend/images/logo-text.png')}}" alt="mkvfinance" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text --><span>
                <!-- dark Logo text -->
                <img src="{{URL::asset('/backend/images/big1.jpg')}}" alt="homepage" class="dark-logo" />
                <!-- Light Logo text -->    
                <img src="{{URL::asset('/backend/images/logo-light-text.png')}}" class="light-logo" alt="homepage" /></span> </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <!-- ============================================================== -->
         
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-cog " style="font-size: 2em;"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right scale-up">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                        <div class="u-text">
                                            <h4>{{ ucfirst(Auth::guard('admin')->user()->name) }}</h4>
                                            <p class="text-muted">{{ ucfirst(Auth::guard('admin')->user()->email) }}</p>
                                        </div>
                                    </div>
                                </li>
                                <!-- <li role="separator" class="divider"></li> -->
                                <!-- <li>
                                    <a href="#" class="some other  classes" admin="1"><span class="ti-user"> </span> My Profile</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="#" class="some other  classes"><span class="fa fa-key "> </span> Change Password</a>
                                </li> -->
                                <li role="separator" class="divider"></li>
                                <li>
                                <a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a>

                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                                    
                                </li>
                            </ul>
                        </div>
                    </li>
            </ul>
        </div>
    </nav>
</header>