<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/frontend/images/2-1.png">
    <title>Welcome to Mkv Finance</title>
    <!-- Bootstrap Core CSS -->

    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/bootstrap/bootstrap.min.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/style.css')}}">
    <!-- You can change the theme colors from here -->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/colors/blue.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/toast-master/css/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/plugins/sweetalert/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/fileupload/bootstrap-fileupload.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/plugin/morris.css')}}">
    @yield('css')



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border">
   
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="preloader">
       <svg class="circular" viewBox="25 25 50 50">
          <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
       </svg>
    </div>
    <div id="main-wrapper">
        <!-- ============================================================== -->
       	@include('admin.layouts.header')
       	@include('admin.layouts.sidebar')

        <!-- Page wrapper  -->
         <div class="page-wrapper">

            
                @yield('content')
          

            <footer class="footer">
            Copyright© {{ date('Y')}} MKV Finance
            </footer>
            
        </div>
    </div>
   
    @include('admin.layouts.footer')
    @yield('script')
    @yield('modal')
</body>

</html>