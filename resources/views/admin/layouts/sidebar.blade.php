<aside class="left-sidebar">
            <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- <li class="nav-devider"></li> -->
                <li> 
                    <a class="waves-effect waves-dark" href="{{route('admin.dashboard')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-microsoft"></i><span class="hide-menu">User Management<span class="label label-rouded label-themecolor pull-right">1</span></span></a>
                    <ul aria-expanded="false" class="collapse {{ (request()->is('admin/account/*')) ? 'in' : '' }} ">
                        <!-- <li><a class="waves-effect" href="{{route('users.index')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> Users </span></a></li> -->   
                        <li class="{{ (request()->is('admin/account/*')) ? 'active' : '' }}"><a class="waves-effect {{ (request()->is('admin/account/*')) ? 'active' : '' }}" href="{{route('account.index')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> Users </span></a></li>
                    </ul>                     
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-microsoft"></i><span class="hide-menu">Master<span class="label label-rouded label-themecolor pull-right">3</span></span></a>
                    <ul aria-expanded="false" class="collapse {{ (request()->is('admin/agent/*')) ? 'in' : '' }} ">
                        <!-- <li><a class="waves-effect" href="{{route('users.index')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> Users </span></a></li> -->   
                        <li class="{{ (request()->is('admin/agent/*')) ? 'active' : '' }}"><a class="waves-effect {{ (request()->is('admin/agent/*')) ? 'active' : '' }}" href="{{route('agent.index')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> Agents </span></a></li>
                        <li class="{{ (request()->is('admin/bank/*')) ? 'active' : '' }}"><a class="waves-effect {{ (request()->is('admin/bank/*')) ? 'active' : '' }}" href="{{route('bank.index')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> Banks </span></a></li>
                        <li class="{{ (request()->is('admin/company/*')) ? 'active' : '' }}"><a class="waves-effect {{ (request()->is('admin/company/*')) ? 'active' : '' }}" href="{{route('company.index')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span> Company </span></a></li>
                    </ul>                     
                </li>               
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
