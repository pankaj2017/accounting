<form action="#" method="post" id="update_preferences_people_post">
    @csrf
    <div class="modal-body p-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <button type="button" class="close py-1 px-2 mr-3 mt-4 editFirstModal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="px-4 py-2 border-bottom"><h2 class="my-3">Update Payment Status</h2></div>
                <div class="py-4" style="max-height: calc(100vh - 250px);overflow-y: auto;overflow-x: hidden;">
                    {{ Form::hidden('account_id', $accountId) }}
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group mb-2">
                                        <label for="">Select Status</label>
                                        <span></span>
                                        <select name="payment_status" required class="form-control form-control-lg">
                                            <option value="0" {{ $payment_status == 0 ? 'selected' : ''}} >Un Paid</option>
                                            <option value="1" {{ $payment_status == 1 ? 'selected' : ''}}>Paid</option>
                                            <option value="2" {{ $payment_status == 2 ? 'selected' : ''}}>By Pass</option>
                                        </select>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="pt-4 border-top">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-block rounded-pill btn-primary-blue py-2 mb-4 font-18">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal-footer d-none">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
</div>
    
