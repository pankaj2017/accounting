@extends('admin.layouts.layout')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Users</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Users </li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Create User</h4>
                </div>
                <div class="card-body">
                	{{ Form::open(array('url' => 'admin/users', 'method' => 'post','class' =>'floating-labels')) }}
                	@csrf  
                	<div class="row p-t-20">
	                	<div class="form-group m-b-40 col-md-6 @if($errors->has('name')) has-danger @endif">
		                    <!-- <input type="text" class="form-control" id="input3">
	 -->
	 						 <?=Form::text('name', old('name'), ['class' => 'form-control'])?>
	 	                    <span class="bar"></span>
		                    <label for="input3">Placeholder</label>
		                    <span class='text-danger error'>{{ $errors->first('name') }}</span>
	                    </div>
	                    <div class="form-group m-b-40 col-md-6 @if($errors->has('name')) has-danger @endif">
		                    <!-- <input type="text" class="form-control" id="input3">
	 -->
	 						 <?=Form::text('name', old('name'), ['class' => 'form-control'])?>
	 	                    <span class="bar"></span>
		                    <label for="input3">Placeholder</label>
		                    <span class='text-danger error'>{{ $errors->first('name') }}</span>
	                    </div>
	                </div>
                    <div class="row p-t-20">
				        <div class="col-md-12">
				            <div class="form-group @if($errors->has('name')) has-danger @endif">
				                <!-- <label class="control-label">Name</label> -->
				                <?=Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Enter name'])?>
				                <span class='text-danger error'>{{ $errors->first('name') }}</span>
				            </div>
				        </div>
				    </div>
					<div class="row p-t-20">
						<div class="col-md-12">
				           <div class="form-group @if($errors->has('email'))has-danger @endif">
				                <label class="control-label">Email</label>
				                <?=Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Enter email'])?>
				                <span class='text-danger error'>{{ $errors->first('email') }}</span>
				            </div>
				        </div>
					</div>
					<div class="row p-t-20">
						<div class="col-md-12">
				           <div class="form-group @if($errors->has('password'))has-danger @endif">
				                <label class="control-label">Password</label>
				                <?=Form::text('password',old('password'), ['type' => 'password','class' => 'form-control', 'placeholder' => 'Enter password'])?>
				                <span class='text-danger error'>{{ $errors->first('password') }}</span>
				            </div>
				        </div>
					</div>
					<div class="row p-t-20">
						<div class="col-md-12">
				           <div class="form-group @if($errors->has('password'))has-danger @endif">
				                <label class="control-label">Confirm Password</label>
				                <?=Form::text('password_confirmation', old('password_confirmation'),['type' => 'password','class' => 'form-control', 'placeholder' => 'Enter password'])?>
				                <span class='text-danger error'>{{ $errors->first('password') }}</span>
				            </div>
				        </div>
					</div>
				    <!--/row-->
					<div class="form-actions">
					    <button type="submit" class="btn btn-success" value="exit" name="save"> <i class="fa fa-check"></i> Save</button>
					    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save & New</button>
					    <a data-showloading="yes" href="" class="btn btn-inverse">Cancel</a>
					   
					</div>
                    <?=Form::close();?>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
@stop

@section('script')
@include('admin.layouts.alert')
@stop
