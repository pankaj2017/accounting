<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('admin.login');
});

//Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::prefix('admin')->group(function() {
    Route::get('/login', [App\Http\Controllers\Auth\Admin\AdminLoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('/login', [App\Http\Controllers\Auth\Admin\AdminLoginController::class, 'login'])->name('admin.login.submit');
    Route::post('logout/', [App\Http\Controllers\Auth\Admin\AdminLoginController::class, 'logout'])->name('admin.logout');

    Route::post('/reminder', [App\Http\Controllers\Admin\AdminController::class, 'reminder'])->name('reminder');
    Route::post('/re-reminder', [App\Http\Controllers\Admin\AdminController::class, 'reReminder'])->name('re.reminder');
    Route::get('/dashboard', [App\Http\Controllers\Admin\AdminController::class,'index'])->name('admin.dashboard');
    Route::resource('users', 'App\Http\Controllers\Admin\UserController');
    Route::resource('account', 'App\Http\Controllers\Admin\AccountController');
    Route::post('/account/authentication', [App\Http\Controllers\Admin\AccountController::class, 'customerAuth'])->name('account.authentication');
    Route::post('/account/information', [App\Http\Controllers\Admin\AccountController::class, 'customerInformation'])->name('account.information');
    Route::post('/get/otp', [App\Http\Controllers\Admin\AccountController::class, 'getOtp'])->name('get.otp');


    Route::get('/file-export',[App\Http\Controllers\Admin\AccountController::class,'export'])->name('export');
    Route::get('/file-import',[App\Http\Controllers\Admin\AccountController::class,'importView'])->name('import-view');
    Route::post('/import',[App\Http\Controllers\Admin\AccountController::class,'import'])->name('import');
    Route::get('/download-pdf/{id}',[App\Http\Controllers\Admin\AccountController::class,'downloadPDF'])->name('downloadPDF');
    // Route::get('users', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('users.index');
    // Route::get('users', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('users.index');
    Route::resource('agent', 'App\Http\Controllers\Admin\AgentController');
    Route::resource('bank', 'App\Http\Controllers\Admin\BankController');
    Route::resource('company', 'App\Http\Controllers\Admin\CompanyController');

    Route::post('/get-agent-phone',[App\Http\Controllers\Admin\AccountController::class,'getPhone'])->name('get.phone');
    Route::post('/get-bank-name',[App\Http\Controllers\Admin\AccountController::class,'getBank'])->name('get.bank');

    Route::post('crop-image-before-upload-using-croppie', [App\Http\Controllers\Admin\AccountController::class,'uploadCropImage'])->name('cropimage');
    Route::get('payment-status', [App\Http\Controllers\Admin\AccountController::class,'paymentStatusGet'])->name('payment.status.get');
    Route::post('payment-status', [App\Http\Controllers\Admin\AccountController::class,'paymentStatusPost'])->name('payment.status.post');


    Route::get('revise-payment-notify', [App\Http\Controllers\Admin\AccountController::class,'revisePaymentNotifyGet'])->name('revise.paymentnotify.get');
    Route::post('revise-payment-notify', [App\Http\Controllers\Admin\AccountController::class,'revisePaymentNotifyPost'])->name('revise.payment.notify.post');
    Route::delete('account-delete', [App\Http\Controllers\Admin\AccountController::class,'deleteAccount'])->name('account.delete');


}) ;